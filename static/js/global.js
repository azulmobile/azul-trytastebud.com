$(document).ready(function() {
	$('nav').waypoint('sticky');  
	
	$('.slicknav_icon-bar').click(function(){
		$(this).toggleClass('active');
	});    
	
	$('.menu').slicknav({
		label: '',
		duration: 250
	});
	
});
